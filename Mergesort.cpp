﻿#include <iostream>
using namespace std;

void merge(int* array1, int* array2, int* mergearray, int size1, int size2) {
	int i1 = 0;
	int j2 = 0;
	int x = 0;
	for (; x < size1 + size2; x++) {
		if (size1 <= i1 || size2 <= j2) {
			break;
		}
		if (array1[i1] < array2[j2]) {
			mergearray[x] = array1[i1++];
		}
		else {
			mergearray[x] = array2[j2++];
		}
	}
	for (; i1 < size1; i1++) {
		mergearray[x++] = array1[i1];
	}
	for (; j2 < size2; j2++) {
		mergearray[x++] = array2[j2];
	}
	
	
}

void mergesort(int* array, int l, int r) {
	if (l >= r)
		return;
	int mid = (l + r) / 2;
	mergesort(array, l, mid);
	mergesort(array, mid + 1, r);
	int* array1 = new int[r - l + 1];
	merge(array + l, array + mid + 1, array1, mid - l + 1 , r - mid);
	int g = 0;
	for (int i = l; i <= r; i++) {
		array[i] = array1[g];
		g++;
	}
	delete[] array1;
}

void shiftDown(int* array, int size, int index) {
	while (index >= 0) {
		int indexLeftchild = 2 * index + 1;
		int indexRightchild = 2 * index + 2;
		int indexMin = index;

		if (indexLeftchild < size && array[indexLeftchild] < array[indexMin])
			indexMin = indexLeftchild;

		if (indexRightchild < size && array[indexRightchild] < array[indexMin])
			indexMin = indexRightchild;
		if (indexMin != index) {
			std::swap(array[index], array[indexMin]);
			index = indexMin;
		}
		else {
			index = -1;
		}
	}
}



void Build(int* array, int size) {
	for (int i = size - 1; i >= 0; i--) {
		shiftDown(array, size, i);
	}
}

void HeapSorted(int* array, int size) {
	Build(array, size);
	while (size > 1) {
		swap(array[size - 1], array[0]);
		size--;
		shiftDown(array, size, 0);
	}
}


void InsertSort(int* array, int size) {
	for (int j = 1; j < size; j++) {
		int m = array[j];
		int i;
		for (i = j; i > 0 && array[i - 1] < m; i--) {
			array[i] = array[i - 1];
		}
		array[i] = m;
	}
}



int main() {

	int array[10] = { 2, 5, 6, 3, 1, 9, 10, 11, 12, 4 };
	mergesort(array, 0, 9);
	for (int i = 0; i < 10; i++)
		cout << array[i] << endl;
	return 0;
	
}